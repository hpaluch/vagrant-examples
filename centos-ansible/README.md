Here is an example using Ansible to provision mc and vim-enhanced on CentOS 6.4 32bit.

Ansible is (nearly) agentless provision tool.


##Setup

On you **host** (where you run vagrant) run:

	:::bash
	yum install -y ansible

##Run 

	:::bash
	vagrant up
	vagrant ssh

> Note: there is hardcoded private address in Vagrantfile and ansible_hosts

##Bugs

vagrant up show error *Ansible failed to complete successfully*. However everything completed successfully.



