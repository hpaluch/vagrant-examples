Setup of empty two node cluster with Centos 6.4 and Pacemaker

Ansible is (nearly) agentless provision tool.


##Setup

On you **host** (where you run vagrant) run:

	:::bash
	yum install -y ansible

##Run 

	:::bash
	vagrant up
	vagrant ssh

TODO: it is incomplete...

> Note: there is hardcoded private address in Vagrantfile and ansible_hosts

##Bugs

vagrant up show error *Ansible failed to complete successfully*. However everything completed successfully.

##Debug

  You can rerun vagrant from your HOST using these commands:

	ssh-add ~/.vagrant.d/insecure_private_key
	ansible-playbook -i ansible_hosts -u vagrant playbook.yml



##Links

*	http://clusterlabs.org/quickstart-redhat.html


