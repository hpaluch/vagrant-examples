
f=/etc/cluster/cluster.conf
n1=node1
n2=node2

ccs -f $f --createcluster pacemaker1 
ccs -f $f --addnode $n1 
ccs -f $f --addnode $n2 

ccs -f $f --addfencedev pcmk agent=fence_pcmk 
ccs -f $f --addmethod pcmk-redirect $n1 
ccs -f $f --addmethod pcmk-redirect $n2 
ccs -f $f --addfenceinst pcmk $n1 pcmk-redirect port=$n1 
ccs -f $f --addfenceinst pcmk $n2 pcmk-redirect port=$n2

