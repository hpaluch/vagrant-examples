###Vagrant sample projects

Here are my simple projects using Vagrant tool.

Vagrant allows to very quickly create and provision Virtual Machines.

Once it is installed you need just "vagrant up" command to create and
setup virtuals from these projects, for example:

	:::bash
	cd hello
	vagrant up
	# once prompt returns log to your new virtual
	vagrant ssh
	# ...
	exit
	vagrant destroy
	
See [Vagrant Site](http://www.vagrantup.com) for more information about Vagrant.


##Fixing Vagrant on CentOS 6.x

If you encounter crash of vagrant on CentOS, please see: [https://github.com/mitchellh/vagrant/pull/2008](https://github.com/mitchellh/vagrant/pull/2008)





